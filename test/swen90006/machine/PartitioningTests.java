package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class PartitioningTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

  //Any method annotation with "@Test" is executed as a test.
  @Test public void aTest()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
    final int expected = 2;
    final int actual = 1 + 1;
    assertEquals(expected, actual);
  }

  @Test public void anotherTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("a");
    list.add("b");

    //the assertTrue method is used to check whether something holds.
    assertTrue(list.contains("a"));
  }

   //Test test opens a file and executes the machine
   @Test public void aFileOpenTest()
  {
  final List<String> lines = readInstructions("examples/array.s");
  Machine m = new Machine();
  assertEquals(m.execute(lines), 45);
  }

  //To test an exception, specify the expected exception after the @Test
  @Test(expected = java.io.IOException.class) 
    public void anExceptionTest()
    throws Throwable
  {
    throw new java.io.IOException();
  }

  //This test should fail.
  //To provide additional feedback when a test fails, an error message
  //can be included
//  @Test public void aFailedTest()
//  {
//    //include a message for better feedback
//    final int expected = 2;
//    final int actual = 1 + 2;
//    assertEquals("Some failure message", expected, actual);
//  }

  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }

  @Test(expected = NoReturnValueException.class)
  public void EC1Test(){
    final List<String> lines = readInstructions("examples/PTest/empty.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  @Test (timeout = 5000)
  public void EC211Test()
  {
    final List<String> lines = readInstructions("examples/PTest/EC211.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }
  @Test(expected = InvalidInstructionException.class)
  public void EC212Test() {
    final List<String> lines = readInstructions("examples/PTest/EC212.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  @Test(expected = NoReturnValueException.class)
  public void EC22Test(){
    final List<String> lines = readInstructions("examples/PTest/EC22.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  @Test(expected = NoReturnValueException.class)
  public void EC32Test(){
    final List<String> lines = readInstructions("examples/PTest/EC32.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  @Test(timeout = 5000)
  public void EC3111ATest()
  {
    final List<String> lines = readInstructions("examples/PTest/EC3111A.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 3);
  }
  @Test(timeout = 5000)
  public void EC3111STest()
  {
    final List<String> lines = readInstructions("examples/PTest/EC3111S.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 1);
  }
  @Test(timeout = 5000)
  public void EC3111MTest()
  {
    final List<String> lines = readInstructions("examples/PTest/EC3111M.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 2);
  }
  @Test(timeout = 5000)
  public void EC3111DTest()
  {
    final List<String> lines = readInstructions("examples/PTest/EC3111D.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 2);
  }
  @Test(expected = InvalidInstructionException.class)
  public void EC3112Test() {
    final List<String> lines = readInstructions("examples/PTest/EC3112.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  @Test(expected = InvalidInstructionException.class)
  public void EC31211Test() {
    final List<String> lines = readInstructions("examples/PTest/EC31211.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  @Test(timeout = 5000)
  public void EC31212Test()
  {
    final List<String> lines = readInstructions("examples/PTest/EC31212.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 1);
  }
  @Test(expected = InvalidInstructionException.class)
  public void EC31213Test() {
    final List<String> lines = readInstructions("examples/PTest/EC31213.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  @Test(expected = InvalidInstructionException.class)
  public void EC3122Test() {
  final List<String> lines = readInstructions("examples/PTest/EC3122.s");
  Machine m = new Machine();
  m.execute(lines);
  }
  @Test(timeout = 5000)
  public void EC31311Test() {
    final List<String> lines = readInstructions("examples/PTest/EC31311.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }
  @Test(timeout = 5000)
  public void EC31312Test() {
    final List<String> lines = readInstructions("examples/PTest/EC31312.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }
  @Test(timeout = 5000)
  public void EC31313Test() {
    final List<String> lines = readInstructions("examples/PTest/EC31313.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }
  @Test(expected = InvalidInstructionException.class)
  public void EC3132Test() {
    final List<String> lines = readInstructions("examples/PTest/EC3132.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  @Test(expected = NoReturnValueException.class)
  public void EC3141Test() {
    final List<String> lines = readInstructions("examples/PTest/EC3141.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  @Test(timeout = 5000)
  public void EC31421Test() {
    final List<String> lines = readInstructions("examples/PTest/EC31421.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }
  // INFINITE LOOP
//  @Test(timeout = 5000)
//  public void EC31422Test() {
//    final List<String> lines = readInstructions("examples/PTest/EC31422.s");
//    Machine m = new Machine();
//    assertEquals(m.execute(lines), 0);
//  }
  @Test(expected = NoReturnValueException.class)
  public void EC3143Test() {
    final List<String> lines = readInstructions("examples/PTest/EC3143.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  // INFINITE LOOP
  @Test(expected = NoReturnValueException.class)
  public void EC31511Test() {
    final List<String> lines = readInstructions("examples/PTest/EC31511.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  @Test(timeout = 5000)
  public void EC31512Test() {
    final List<String> lines = readInstructions("examples/PTest/EC31512.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }
  @Test(expected = NoReturnValueException.class)
  public void EC31513Test() {
    final List<String> lines = readInstructions("examples/PTest/EC31513.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  @Test(expected = InvalidInstructionException.class)
  public void EC3152Test() {
    final List<String> lines = readInstructions("examples/PTest/EC3152.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  @Test(expected = InvalidInstructionException.class)
  public void EC316Test() {
    final List<String> lines = readInstructions("examples/PTest/EC316.s");
    Machine m = new Machine();
    m.execute(lines);
  }


}
