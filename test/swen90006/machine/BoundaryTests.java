package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

  //Any method annotation with "@Test" is executed as a test.
  @Test public void aTest()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
    final int expected = 2;
    final int actual = 1 + 1;
    assertEquals(expected, actual);
  }

  @Test public void anotherTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("a");
    list.add("b");

    //the assertTrue method is used to check whether something holds.
    assertTrue(list.contains("a"));
  }
  //Test test opens a file and executes the machine
  @Test public void aFileOpenTest()
  {
  final List<String> lines = readInstructions("examples/array.s");
  Machine m = new Machine();
  assertEquals(m.execute(lines), 45);
  }
  
  //To test an exception, specify the expected exception after the @Test
  @Test(expected = java.io.IOException.class) 
    public void anExceptionTest()
    throws Throwable
  {
    throw new java.io.IOException();
  }

  //This test should fail.
  //To provide additional feedback when a test fails, an error message
  //can be included
//  @Test public void aFailedTest()
//  {
//    //include a message for better feedback
//    final int expected = 2;
//    final int actual = 1 + 2;
//    assertEquals("Some failure message", expected, actual);
//  }

  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }

  @Test(expected = NoReturnValueException.class)
  public void B1Test() {
    final List<String> lines = readInstructions("examples/BTest/B1.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  @Test(timeout = 5000)
  public void B2Test() {
    final List<String> lines = readInstructions("examples/BTest/B2.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }
  @Test(timeout = 5000)
  public void B3Test() {
    final List<String> lines = readInstructions("examples/BTest/B3.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 3);
  }
  @Test(timeout = 5000)
  public void B4Test() {
    final List<String> lines = readInstructions("examples/BTest/B4.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }
  @Test(expected = NoReturnValueException.class)
  public void B5Test() {
    final List<String> lines = readInstructions("examples/BTest/B5.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  @Test(timeout = 5000)
  public void B6Test() {
    final List<String> lines = readInstructions("examples/BTest/B6.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }
  @Test(timeout = 5000)
  public void B7Test() {
  final List<String> lines = readInstructions("examples/BTest/B7.s");
  Machine m = new Machine();
  assertEquals(m.execute(lines), 0);
  }
  @Test(expected = InvalidInstructionException.class)
  public void B8Test() {
    final List<String> lines = readInstructions("examples/BTest/B8.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  @Test(expected = InvalidInstructionException.class)
  public void B9Test() {
    final List<String> lines = readInstructions("examples/BTest/B9.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  @Test(timeout = 5000)
  public void B10Test() {
    final List<String> lines = readInstructions("examples/BTest/B10.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 2);
  }
  @Test(expected = NoReturnValueException.class)
  public void B11Test() {
    final List<String> lines = readInstructions("examples/BTest/B11.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  @Test(expected = InvalidInstructionException.class)
  public void B12Test() {
    final List<String> lines = readInstructions("examples/BTest/B12.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  @Test(timeout = 5000)
  public void B13Test() {
    final List<String> lines = readInstructions("examples/BTest/B13.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), -65535);
  }
  @Test(timeout = 5000)
  public void B14Test() {
    final List<String> lines = readInstructions("examples/BTest/B14.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 65535);
  }
  @Test(expected = InvalidInstructionException.class)
  public void B15Test() {
    final List<String> lines = readInstructions("examples/BTest/B15.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  @Test(timeout = 5000)
  public void B16Test() {
    final List<String> lines = readInstructions("examples/BTest/B16.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines),0);
  }
  @Test(timeout = 5000)
  public void B17Test() {
    final List<String> lines = readInstructions("examples/BTest/B17.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }
  @Test(timeout = 5000)
  public void B18Test() {
    final List<String> lines = readInstructions("examples/BTest/B18.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }
  @Test(timeout = 5000)
  public void B19Test() {
    final List<String> lines = readInstructions("examples/BTest/B19.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }
  @Test(expected = NoReturnValueException.class)
  public void B20Test() {
    final List<String> lines = readInstructions("examples/BTest/B20.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  //INFINITE LOOP
//  @Test(timeout = 5000)
//  public void B21Test() {
//    final List<String> lines = readInstructions("examples/BTest/B21.s");
//    Machine m = new Machine();
//    assertEquals(m.execute(lines), 0);
//  }
  @Test(timeout = 5000)
  public void B22Test() {
    final List<String> lines = readInstructions("examples/BTest/B22.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }
  @Test(expected = NoReturnValueException.class)
  public void B23Test() {
    final List<String> lines = readInstructions("examples/BTest/B23.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  @Test(expected = NoReturnValueException.class)
  public void B24Test() {
    final List<String> lines = readInstructions("examples/BTest/B24.s");
    Machine m = new Machine();
    m.execute(lines);
  }
  //INFINITE LOOP
//  @Test(timeout = 5000)
//  public void B25Test() {
//    final List<String> lines = readInstructions("examples/BTest/B25.s");
//    Machine m = new Machine();
//    assertEquals(m.execute(lines), 0);
//  }
  @Test(timeout = 5000)
  public void B26Test() {
    final List<String> lines = readInstructions("examples/BTest/B26.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }
  @Test(expected = NoReturnValueException.class)
  public void B27Test() {
    final List<String> lines = readInstructions("examples/BTest/B27.s");
    Machine m = new Machine();
    m.execute(lines);
  }

}
